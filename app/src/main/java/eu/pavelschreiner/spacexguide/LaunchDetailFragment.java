package eu.pavelschreiner.spacexguide;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import eu.pavelschreiner.spacexguide.pojo.launch.Launch;

/**
 * Created by west on 7.1.2018.
 */

public class LaunchDetailFragment extends Fragment {
    private Launch clickedLaunch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launch_detail, container, false);
        Gson gson = new Gson();
        clickedLaunch = gson.fromJson(getArguments().getString("clickedLaunch"), Launch.class);
        initWidgets(view);
        return view;
    }

    private void initWidgets(View view) {
        TextView details = (TextView) view.findViewById(R.id.launch_details);
        TextView articleLink = (TextView) view.findViewById(R.id.launch_article_link);
        TextView missionPatchLink = (TextView) view.findViewById(R.id.launch_missionpatch_link);
        TextView pressKit = (TextView) view.findViewById(R.id.launch_presskit_link);
        details.setText(clickedLaunch.getDetails());
        articleLink.setText(clickedLaunch.getLinks().getArticle_link());
        missionPatchLink.setText(clickedLaunch.getLinks().getMissionPatch());
        pressKit.setText(clickedLaunch.getLinks().getPresskit());
    }
}

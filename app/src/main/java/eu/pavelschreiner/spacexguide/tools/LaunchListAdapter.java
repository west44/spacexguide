package eu.pavelschreiner.spacexguide.tools;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import eu.pavelschreiner.spacexguide.LaunchDetailFragment;
import eu.pavelschreiner.spacexguide.MainActivity;
import eu.pavelschreiner.spacexguide.R;
import eu.pavelschreiner.spacexguide.pojo.launch.Launch;


public class LaunchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = LaunchListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<Launch> launches;
    private static final int EMPTY_VIEW = 10;

    public LaunchListAdapter(Context context) {
        this.context = context;
        launches = new ArrayList<>();
    }

    public void addLaunches(List<Launch> launchesList, Boolean clearBeforeAdd) {
        if (clearBeforeAdd) launches.clear();
        launches.addAll(launchesList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == EMPTY_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_row, parent, false);
            EmptyViewHolder evh = new EmptyViewHolder(view);
            return evh;
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.launch_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) { // use only if there are some launches to show
            ViewHolder viewHolder = (ViewHolder) holder;
            Launch Launch = launches.get(position);
            if (Launch.getFlightNumber() > 0) viewHolder.launchId.setText(String.valueOf(Launch.getFlightNumber()));
            if (Launch.getLaunchDate() != null)  viewHolder.launchDate.setText(Launch.getLaunchDate());
            if (Launch.getRocket() != null)  viewHolder.launchRocket.setText(Launch.getRocket().getRocketName());
            if (Launch.isLaunchSuccess()) viewHolder.itemView.setBackgroundResource(R.color.colorLaunchSuccess);
            else viewHolder.itemView.setBackgroundResource(R.color.colorLaunchFailed);
            viewHolder.itemView.setTag(launches.get(position));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showLaunchDetail(position);
                }
            });
        }
    }

    public void showLaunchDetail(int position) {
        Gson gson = new Gson();
        LaunchDetailFragment detailFragment = new LaunchDetailFragment();
        Bundle args = new Bundle();
        args.putString("clickedLaunch", gson.toJson(launches.get((int) getItemId(position))));
        detailFragment.setArguments(args);
        FragmentTransaction transaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, detailFragment);
        transaction.addToBackStack("clickedLaunch");
        transaction.commit();
    }

    @Override
    public int getItemViewType(int position) {
        if (launches.isEmpty()) return EMPTY_VIEW; // no launches at this time
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        if (launches.size() == 0) return 1; // 1 because it will show one row with empty text message
        else return launches.size();
    }

    // use for regular launches
    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView launchId, launchRocket, launchDate;

        ViewHolder(View view) {
            super(view);
            launchId = (TextView) view.findViewById(R.id.launch_id);
            launchDate = (TextView) view.findViewById(R.id.launch_date);
            launchRocket = (TextView) view.findViewById(R.id.launch_rocket);
        }
    }

    // use for empty message when there is no launches
    class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }
}

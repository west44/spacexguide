package eu.pavelschreiner.spacexguide.tools.network;

import java.util.List;

import eu.pavelschreiner.spacexguide.pojo.launch.Launch;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by west on 7.1.2018.
 */

public class SpaceXAdapter {
    private static final String TAG = SpaceXAdapter.class.getSimpleName();
    private Retrofit retrofit;
    private SpaceXApi spaceXApi;
    private static final String API_URL = "https://api.spacexdata.com";

    public SpaceXAdapter() {
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        spaceXApi = retrofit.create(SpaceXApi.class);
    }

    public Call<List<Launch>> getLaunches(int year, String rocketId) {
        String launchYear = (year == 0) ? null : String.valueOf(year);
        return spaceXApi.getLaunchesData(launchYear, rocketId);
    }
}

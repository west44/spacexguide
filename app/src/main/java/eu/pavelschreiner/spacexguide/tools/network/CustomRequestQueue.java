package eu.pavelschreiner.spacexguide.tools.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

// CustomRequestQueue using design pattern Singleton
public class CustomRequestQueue {
    private static final int CACHE_MAX_SIZE = 20;
    private static CustomRequestQueue instance;
    private RequestQueue requestQueue;
    private Context context;
    private ImageLoader imageLoader;

    private CustomRequestQueue(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(this.requestQueue,
            new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(CACHE_MAX_SIZE);

                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) requestQueue = Volley.newRequestQueue(context);
        return requestQueue;
    }

    public static synchronized CustomRequestQueue getInstance(Context context) {
        if (instance == null) instance = new CustomRequestQueue(context);
        return instance;
    }

    public <T> void addRequest(Request<T> request) {
        requestQueue.add(request);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}

package eu.pavelschreiner.spacexguide.tools;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import eu.pavelschreiner.spacexguide.CompanyFragment;
import eu.pavelschreiner.spacexguide.R;
import eu.pavelschreiner.spacexguide.pojo.launch.Launch;
import eu.pavelschreiner.spacexguide.pojo.launch.LaunchRocket;
import eu.pavelschreiner.spacexguide.pojo.company.Company;
import eu.pavelschreiner.spacexguide.tools.network.CustomRequestQueue;
import eu.pavelschreiner.spacexguide.tools.network.GsonRequest;
import eu.pavelschreiner.spacexguide.tools.network.SpaceXAdapter;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by west on 6.1.2018.
 */

public class DataLoader {
    private static final String TAG = DataLoader.class.getSimpleName();
    private static final String COMPANY_URL = "https://api.spacexdata.com/v2/info";
    private LaunchListAdapter launchListAdapter;
    private SpaceXAdapter spaceXAdapter;
    private Context context;
    private int launchYear;
    private LaunchRocket rocket;
    private ArrayList<Integer> validYears = new ArrayList<>();
    private ArrayList<LaunchRocket> validRockets = new ArrayList<>();
    private Callback<List<Launch>> callback = new Callback<List<Launch>>() {
        @Override
        public void onResponse(Call<List<Launch>> call, retrofit2.Response<List<Launch>> response) {
            if (response.isSuccessful()) {
                launchListAdapter.addLaunches(response.body(), true);
                if (validRockets.isEmpty() || validYears.isEmpty()) setValidFilterData(response.body());
            }
        }

        @Override
        public void onFailure(Call<List<Launch>> call, Throwable t) {
            Log.d(TAG, "Error: " + t.getMessage());
        }
    };

    public DataLoader(Context context) {
        this(context, null);
    }

    public DataLoader(Context context, LaunchListAdapter launchListAdapter) {
        this.context = context;
        if (launchListAdapter != null) {
            this.launchListAdapter = launchListAdapter;
            spaceXAdapter = new SpaceXAdapter();
        }
    }

    public String getCompanyData(final CompanyFragment companyFragment) {
//        Log.i("DATA_SOURCE", "getData() starts, " + generateUrl(page));
        GsonRequest gsonRequest = new GsonRequest<Company>(
                COMPANY_URL,
                Company.class,
                null,
                new Response.Listener<Company>() {
                    public void onResponse(Company response) {
                        companyFragment.setComapanyDetails(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(companyFragment.getContext(), R.string.error_data_loading, Toast.LENGTH_LONG).show();
                    }
                }
        );
        CustomRequestQueue.getInstance(context).addRequest(gsonRequest);
        return gsonRequest.toString();
    }

    public void getLaunchesData() {
        Call<List<Launch>> call = spaceXAdapter.getLaunches(launchYear, (rocket == null) ? null : rocket.getRocketId());
        call.enqueue(callback);
    }

    public void setLaunchYear(int year) {
        this.launchYear = year;
    }

    public Integer getLaunchYear() {
        return launchYear;
    }

    public void setRocketId(LaunchRocket rocket) {
        this.rocket = rocket;
    }

    public LaunchRocket getRocket() {
        return rocket;
    }

    private void setValidFilterData(List<Launch> launches) {
        for (Launch launch : launches) {
            if (!validYears.contains(launch.getLaunchYear())) validYears.add(launch.getLaunchYear());
            if (!validRockets.contains(launch.getRocket())) validRockets.add(launch.getRocket());
        }
    }

    public ArrayList<Integer> getValidYears() {
        return validYears;
    }

    public ArrayList<LaunchRocket> getValidRockets() {
        return validRockets;
    }
}

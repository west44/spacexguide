package eu.pavelschreiner.spacexguide.tools.network;

import java.util.List;

import eu.pavelschreiner.spacexguide.pojo.launch.Launch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by west on 7.1.2018.
 */

public interface SpaceXApi {
    @GET("v2/launches")
    //void getLaunchesData(@Query("launch_year") int year, Callback<Launch> callback);
    Call<List<Launch>> getLaunchesData(@Query("launch_year") String year, @Query("rocket_id") String rocketId);
}

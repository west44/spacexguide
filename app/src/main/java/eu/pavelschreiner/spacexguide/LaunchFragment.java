package eu.pavelschreiner.spacexguide;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


import eu.pavelschreiner.spacexguide.pojo.launch.LaunchRocket;
import eu.pavelschreiner.spacexguide.tools.DataLoader;
import eu.pavelschreiner.spacexguide.tools.LaunchListAdapter;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by west on 6.1.2018.
 */

public class LaunchFragment extends Fragment {
    private static final String TAG = LaunchFragment.class.getSimpleName();
    private View view;
    private LaunchListAdapter launchListAdapter;
    private DataLoader dataLoader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_launch_list, container, false);
        setHasOptionsMenu(true);
        launchListAdapter = new LaunchListAdapter(getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.launch_list);
        recyclerView.setAdapter(launchListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataLoader = new DataLoader(getContext(), launchListAdapter);
        dataLoader.getLaunchesData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.launch_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_filter) {
            showFilterDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_filter);
        
        final MaterialSpinner yearSpinner = (MaterialSpinner) dialog.findViewById(R.id.spinner_year);
        final MaterialSpinner rocketSpinner = (MaterialSpinner) dialog.findViewById(R.id.spinner_rocket);

        ArrayAdapter<Integer> yearSpinnerAdapter = new ArrayAdapter<Integer>(
                getContext(), android.R.layout.simple_spinner_item, dataLoader.getValidYears());
        ArrayAdapter<LaunchRocket> rocketSpinnerAdapter = new ArrayAdapter<LaunchRocket>(
                getContext(), android.R.layout.simple_spinner_item, dataLoader.getValidRockets());

        yearSpinner.setAdapter(yearSpinnerAdapter);
        rocketSpinner.setAdapter(rocketSpinnerAdapter);
        yearSpinner.setSelection(yearSpinnerAdapter.getPosition(dataLoader.getLaunchYear()));
        rocketSpinner.setSelection(rocketSpinnerAdapter.getPosition(dataLoader.getRocket()));

        Button dialogButton = (Button) dialog.findViewById(R.id.apply_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataLoader.setLaunchYear((int) yearSpinner.getSelectedItem());
                dataLoader.setRocketId((LaunchRocket) rocketSpinner.getSelectedItem());
                dataLoader.getLaunchesData();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

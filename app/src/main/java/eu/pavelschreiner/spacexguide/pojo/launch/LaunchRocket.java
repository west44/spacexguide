package eu.pavelschreiner.spacexguide.pojo.launch;

import android.util.Log;

/**
 * Created by west on 6.1.2018.
 */

public class LaunchRocket {

    /**
     * rocket_id : falcon9
     * rocket_name : Falcon 9
     * rocket_type : FT
     * first_stage : {"cores":[{"core_serial":"B1036","reused":true,"land_success":false,"landing_type":null,"landing_vehicle":null}]}
     * second_stage : {"payloads":[{"payload_id":"Iridium NEXT 4","reused":false,"customers":["Iridium Communications"],"payload_type":"Satellite","payload_mass_kg":9600,"payload_mass_lbs":21164.38,"orbit":"LEO"}]}
     */

    private String rocket_id;
    private String rocket_name;
    private String rocket_type;

    public String getRocketId() {
        return rocket_id;
    }

    public void setRocketId(String rocket_id) {
        this.rocket_id = rocket_id;
    }

    public String getRocketName() {
        return rocket_name;
    }

    public void setRocketName(String rocket_name) {
        this.rocket_name = rocket_name;
    }

    public String getRocketType() {
        return rocket_type;
    }

    public void setRocketType(String rocket_type) {
        this.rocket_type = rocket_type;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return rocket_id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LaunchRocket){
            LaunchRocket c = (LaunchRocket) obj;
            if (c.getRocketId().equals(rocket_id)) return true;
        }
        return false;
    }
}

package eu.pavelschreiner.spacexguide.pojo.launch;

/**
 * Created by west on 6.1.2018.
 */

public class LaunchLinks {

    /**
     * mission_patch : https://i.imgur.com/bNwARXL.png
     * reddit_campaign : https://www.reddit.com/r/spacex/comments/7cgts7/iridium_next_constellation_mission_4_launch/
     * reddit_launch : https://www.reddit.com/r/spacex/comments/7li8y2/rspacex_iridium_next_4_official_launch_discussion/
     * reddit_recovery : null
     * reddit_media : https://www.reddit.com/r/spacex/comments/7litv2/rspacex_iridium4_media_thread_videos_images_gifs/
     * presskit : http://www.spacex.com/sites/spacex/files/iridium4presskit.pdf
     * article_link : https://spaceflightnow.com/2017/12/23/spacex-launch-dazzles-delivering-10-more-satellites-for-iridium/
     * video_link : https://www.youtube.com/watch?v=wtdjCwo6d3Q
     */

    private String mission_patch;
    private String presskit;
    private String article_link;
    private String video_link;

    public String getMissionPatch() {
        return mission_patch;
    }

    public void setMissionPatch(String mission_patch) {
        this.mission_patch = mission_patch;
    }

    public String getPresskit() {
        return presskit;
    }

    public void setPresskit(String presskit) {
        this.presskit = presskit;
    }

    public String getArticle_link() {
        return article_link;
    }

    public void setArticle_link(String article_link) {
        this.article_link = article_link;
    }

    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }
}

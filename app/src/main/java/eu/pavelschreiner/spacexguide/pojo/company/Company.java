package eu.pavelschreiner.spacexguide.pojo.company;

/**
 * Created by west on 6.1.2018.
 */

public class Company {

    /**
     * name : SpaceX
     * founder : Elon Musk
     * founded : 2002
     * employees : 7000
     * vehicles : 3
     * launch_sites : 3
     * test_sites : 1
     * ceo : Elon Musk
     * cto : Elon Musk
     * coo : Gwynne Shotwell
     * cto_propulsion : Tom Mueller
     * valuation : 15000000000
     * headquarters : {"address":"Rocket Road","city":"Hawthorne","state":"California"}
     * summary : SpaceX designs, manufactures and launches advanced rockets and spacecraft. The company was founded in 2002 to revolutionize space technology, with the ultimate goal of enabling people to live on other planets.
     */

    private String name;
    private String founder;
    private int founded;
    private int employees;
    private String ceo;
    private String cto;
    private String coo;
    private long valuation;
    private CompanyHeadquarters headquarters;
    private String summary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public int getFounded() {
        return founded;
    }

    public void setFounded(int founded) {
        this.founded = founded;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public String getCeo() {
        return ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getCto() {
        return cto;
    }

    public void setCto(String cto) {
        this.cto = cto;
    }

    public String getCoo() {
        return coo;
    }

    public void setCoo(String coo) {
        this.coo = coo;
    }

    public long getValuation() {
        return valuation;
    }

    public void setValuation(long valuation) {
        this.valuation = valuation;
    }

    public CompanyHeadquarters getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(CompanyHeadquarters headquarters) {
        this.headquarters = headquarters;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public static class CompanyHeadquarters {
        /**
         * address : Rocket Road
         * city : Hawthorne
         * state : California
         */

        private String address;
        private String city;
        private String state;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}

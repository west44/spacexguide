package eu.pavelschreiner.spacexguide.pojo.launch;

/**
 * Created by west on 6.1.2018.
 */

public class Launch {

    /**
     * flight_number : 52
     * launch_year : 2017
     * launch_date_unix : 1513992443
     * launch_date_utc : 2017-12-23T01:27:23Z
     * launch_date_local : 2017-12-22T17:27:23-08:00
     * rocket : {"rocket_id":"falcon9","rocket_name":"Falcon 9","rocket_type":"FT","first_stage":{"cores":[{"core_serial":"B1036","reused":true,"land_success":false,"landing_type":null,"landing_vehicle":null}]},"second_stage":{"payloads":[{"payload_id":"Iridium NEXT 4","reused":false,"customers":["Iridium Communications"],"payload_type":"Satellite","payload_mass_kg":9600,"payload_mass_lbs":21164.38,"orbit":"LEO"}]}}
     * telemetry : {"flight_club":"https://www.flightclub.io/result?code=IRD4"}
     * reuse : {"core":true,"side_core1":false,"side_core2":false,"fairings":false,"capsule":false}
     * launch_site : {"site_id":"vafb_slc_4e","site_name":"VAFB SLC 4E","site_name_long":"Vandenberg Air Force Base Space Launch Complex 4E"}
     * launch_success : true
     * links : {"mission_patch":"https://i.imgur.com/bNwARXL.png","reddit_campaign":"https://www.reddit.com/r/spacex/comments/7cgts7/iridium_next_constellation_mission_4_launch/","reddit_launch":"https://www.reddit.com/r/spacex/comments/7li8y2/rspacex_iridium_next_4_official_launch_discussion/","reddit_recovery":null,"reddit_media":"https://www.reddit.com/r/spacex/comments/7litv2/rspacex_iridium4_media_thread_videos_images_gifs/","presskit":"http://www.spacex.com/sites/spacex/files/iridium4presskit.pdf","article_link":"https://spaceflightnow.com/2017/12/23/spacex-launch-dazzles-delivering-10-more-satellites-for-iridium/","video_link":"https://www.youtube.com/watch?v=wtdjCwo6d3Q"}
     * details : Reusing the booster first used on Iridium-2, but will be flying expendable.
     */

    private int flight_number;
    private int launch_year;
    private String launch_date_utc;
    private LaunchRocket rocket;
    private boolean launch_success;
    private LaunchLinks links;
    private String details;

    public int getFlightNumber() {
        return flight_number;
    }

    public void setFlightNumber(int flight_number) {
        this.flight_number = flight_number;
    }

    public int getLaunchYear() {
        return launch_year;
    }

    public void setLaunchYear(int launch_year) {
        this.launch_year = launch_year;
    }

    public String getLaunchDate() {
        return launch_date_utc;
    }

    public void setLaunchDate(String launch_date_utc) {
        this.launch_date_utc = launch_date_utc;
    }

    public LaunchRocket getRocket() {
        return rocket;
    }

    public void setRocket(LaunchRocket rocket) {
        this.rocket = rocket;
    }

    public boolean isLaunchSuccess() {
        return launch_success;
    }

    public void setLaunchSuccess(boolean launch_success) {
        this.launch_success = launch_success;
    }

    public LaunchLinks getLinks() {
        return links;
    }

    public void setLinks(LaunchLinks links) {
        this.links = links;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}

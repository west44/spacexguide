package eu.pavelschreiner.spacexguide;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import eu.pavelschreiner.spacexguide.pojo.company.Company;
import eu.pavelschreiner.spacexguide.tools.DataLoader;
import eu.pavelschreiner.spacexguide.tools.network.CustomRequestQueue;

/**
 * Created by west on 6.1.2018.
 */

public class CompanyFragment extends Fragment {
    private View view;
    private static final String COMPANY_LOGO_URL = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/SpaceX-Logo.svg/500px-SpaceX-Logo.svg.png";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company, container, false);
        NetworkImageView companyLogo = (NetworkImageView) view.findViewById(R.id.company_logo);
        companyLogo.setImageUrl(COMPANY_LOGO_URL, CustomRequestQueue.getInstance(getContext()).getImageLoader());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataLoader dataLoader = new DataLoader(getContext());
        dataLoader.getCompanyData(this);
    }

    public void setComapanyDetails(Company company) {
        TextView companyName = (TextView) view.findViewById(R.id.company_name);
        TextView companyFounder = (TextView) view.findViewById(R.id.company_founder);
        TextView companyFounded = (TextView) view.findViewById(R.id.company_founded);
        TextView companyCEO = (TextView) view.findViewById(R.id.company_ceo);
        TextView companyCTO = (TextView) view.findViewById(R.id.company_cto);
        TextView companyCOO = (TextView) view.findViewById(R.id.company_coo);
        TextView companyEmployees = (TextView) view.findViewById(R.id.company_employees);
        TextView companyValuation = (TextView) view.findViewById(R.id.company_valuation);
        TextView companyHeadquarters = (TextView) view.findViewById(R.id.company_headquarters);

        companyName.setText(company.getName());
        companyFounder.setText(company.getFounder());
        companyFounded.setText(String.valueOf(company.getFounded()));
        companyCEO.setText(company.getCeo());
        companyCTO.setText(company.getCto());
        companyCOO.setText(company.getCoo());
        companyEmployees.setText(String.valueOf(company.getEmployees()));
        companyValuation.setText(String.valueOf(company.getValuation()));
        companyHeadquarters.setText(
            company.getHeadquarters().getAddress() + "\n" +
            company.getHeadquarters().getCity() + "\n" +
            company.getHeadquarters().getState()
        );
    }
}
